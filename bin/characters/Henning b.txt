[ACCOUNT]
character-username = Henning b
character-password = 569348

[CHARACTER]
character-height = 0
character-posx = 2100
character-posy = 3913
character-rights = 3
character-title = 
character-title-color = 0
connected-from = 127.0.0.1
removedTask0 = -1
removedTask1 = -1
removedTask2 = -1
removedTask3 = -1
desert-treasure = 0
dt-kill = 0
horror-from-deep = 0
rfd-round = 0
quest-points = 0
cooks-assistant = 0
lastLoginDate = 0
startPack = false
setPin = false
bankPin1 = 0
bankPin2 = 0
bankPin3 = 0
bankPin4 = 0
hasBankpin = false
pinRegisteredDeleteDay = 0
requestPinDelete = false
slayerPoints =0
crystal-bow-shots = 0
skull-timer = 0
magic-book = 0
brother-info = 0	0
brother-info = 1	0
brother-info = 2	0
brother-info = 3	0
brother-info = 4	0
brother-info = 5	0
special-amount = 10.0
selected-coffin = 0
KC =0
DC =0
pkp = 0
donP =0
xpLock = false
barrows-killcount = 0
teleblock-length = 0
pc-points = 0
mute-end = 0
slayerTask = 0
taskAmount = 0
magePoints = 0
autoRet = 0
barrowskillcount = 0
flagged = false
wave = 0
gwkc = 0
fightMode = 0
void = 0	0	0	0	0
firesLit = 0
crabsKilled = 0

[EQUIPMENT]
character-equip = 0	-1	1	
character-equip = 1	-1	1	
character-equip = 2	-1	1	
character-equip = 3	-1	0	
character-equip = 4	-1	1	
character-equip = 5	-1	1	
character-equip = 6	-1	0	
character-equip = 7	-1	1	
character-equip = 8	-1	0	
character-equip = 9	-1	1	
character-equip = 10	-1	1	
character-equip = 11	-1	0	
character-equip = 12	-1	1	
character-equip = 13	-1	0	

[LOOK]
character-look = 0	0
character-look = 1	0
character-look = 2	18
character-look = 3	26
character-look = 4	33
character-look = 5	36
character-look = 6	42
character-look = 7	10
character-look = 8	0
character-look = 9	0
character-look = 10	0
character-look = 11	0
character-look = 12	0

[SKILLS]
character-skill = 0	1	0
character-skill = 1	1	0
character-skill = 2	1	0
character-skill = 3	10	1300
character-skill = 4	1	0
character-skill = 5	1	0
character-skill = 6	1	0
character-skill = 7	1	0
character-skill = 8	1	0
character-skill = 9	1	0
character-skill = 10	1	0
character-skill = 11	1	0
character-skill = 12	1	0
character-skill = 13	1	0
character-skill = 14	1	0
character-skill = 15	1	0
character-skill = 16	1	0
character-skill = 17	1	0
character-skill = 18	1	0
character-skill = 19	1	0
character-skill = 20	1	0
character-skill = 21	1	0
character-skill = 22	1	0
character-skill = 23	1	0
character-skill = 24	1	0

[ITEMS]

[BANK]
character-bank = 0	6857	1
character-bank = 1	6858	1
character-bank = 2	996	200000
character-bank = 3	1726	1
character-bank = 4	555	1000
character-bank = 5	556	1000
character-bank = 6	557	1000
character-bank = 7	559	1000
character-bank = 8	561	1000
character-bank = 9	566	1000
character-bank = 10	1324	1
character-bank = 11	842	1
character-bank = 12	883	1000
character-bank = 13	10500	1
character-bank = 14	380	200
character-bank = 15	543	1
character-bank = 16	545	1
character-bank = 17	3106	1
character-bank = 18	7459	1
character-bank = 19	591	1
character-bank = 20	1352	1
character-bank = 21	1266	1
character-bank = 22	304	1
character-bank = 23	310	1
character-bank = 24	314	1000
character-bank = 25	306	1
character-bank = 26	312	1
character-bank = 27	302	1

[FRIENDS]

[EOF]

