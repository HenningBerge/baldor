package baldor.model.players;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import baldor.util.SqlHandler;

public class Highscores {
	public static Connection con;
	public static Statement stm;
	public static boolean connected;

	public static String Host = "jdbc:mysql://162.212.253.163/ardirsps_hiscores";
	public static String User = "ardirsps_hisc";
	public static String Pass = "hirahisc";

	public static void process() {
		try {
			SqlHandler.createCon();
			connected = true;
		} catch (Exception e) {
			connected = false;
			e.printStackTrace();
		}
	}

	public static ResultSet query(String s) throws SQLException {
		if (s.toLowerCase().startsWith("select")) {
			ResultSet resultset = stm.executeQuery(s);
			return resultset;
		}
		try {
			stm.executeUpdate(s);
			return null;
		} catch (Exception e) {
			destroy();
		}
		process();
		return null;
	}

	public static void destroy() {
		try {
			if (stm != null)
				stm.close();
			if (con != null)
				con.close();
			connected = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean save(Client c) {
	if(SqlHandler.save(c)){
		return true;
	}
	return false;
	}

	public static String Driver = "com.mysql.jdbc.Driver";
}