package baldor.model.players.packets;

import baldor.Config;
import baldor.Connection;
import baldor.Server;
import baldor.model.npcs.NPCHandler;
import baldor.model.players.Client;
import baldor.model.players.PacketType;
import baldor.model.players.PlayerHandler;
import baldor.util.MadTurnipConnection;
import baldor.util.Misc;
import baldor.util.SqlHandler;

/**
 * Commands
 **/
public class Commands implements PacketType {

	@Override
	public void processPacket(Client c, int packetType, int packetSize) {
		String playerCommand = c.getInStream().readString();
		String outputCommand = playerCommand;
		String outputCommandsubstring = "";


		if(playerCommand.contains(" "))
		{
			String[] theCommand = playerCommand.split(" ");
			outputCommand = theCommand[0].toLowerCase();
			outputCommandsubstring = theCommand[1].toLowerCase();
		} else {
			outputCommand = playerCommand;
		}


		if (Config.SERVER_DEBUG)
			Misc.println(c.playerName + " playerCommand: " + playerCommand);

		
		switch(outputCommand){ //Command Switch statment start


		/**
		*	Yell Command
		*	@HenningBerge
		*/

		case "yell":
			String rank = "";
			String Message = playerCommand.substring(4);
			switch(c.playerRights){
			case 1:
				rank = "[@blu@Moderator@bla@][@blu@" + c.playerName
						+ "@bla@]:@dre@";
				break;
			case 2:
				rank = "[@or3@Administrator@bla@][@blu@"
						+ Misc.ucFirst(c.playerName) + "@bla@]:@dre@";
				break;
			case 3:
				rank = "[@red@Baldor Dev@bla@] @cr2@"
						+ Misc.ucFirst(c.playerName) + ":@dre@";
				break;
			default:
				c.sendMessage("Yell for all enabled during beta.");
				rank = "[@bla@Player@bla@] @bla@"
						+ Misc.ucFirst(c.playerName) + ":@dre@";
				break;
			}
			for (int j = 0; j < PlayerHandler.players.length; j++) {
				if (PlayerHandler.players[j] != null) {
					Client c2 = (Client) PlayerHandler.players[j];
					c2.sendMessage(rank + Message);
				}
			}
		break;
		

		/**
		*	Clan Chat Command
		*	@HenningBerge
		*/

		case "/": 
            if (Connection.isMuted(c))
            {
            	c.sendMessage("You are muted for breaking a rule.");
                return;
            }
			if (c.clan != null) {
				String message = playerCommand.substring(1);
				c.clan.sendChat(c, message);
			} else {
				c.sendMessage("You can only do this in a clan chat..");
			}
		break;
		case "outfit":
			switch(c.playerRights){
			case 3:
			c.playerEquipment[0] = 1048;
			c.playerEquipment[1] = 9777;
			c.playerEquipment[2] = 10354;
			c.playerEquipment[3] = 11230;
			c.playerEquipment[4] = 10416;
			c.playerEquipment[5] = 11283;
			c.playerEquipment[6] = 11696;
			c.playerEquipment[7] = 10418;
			c.playerEquipment[8] = 11696;
			c.playerEquipment[9] = 775;
			c.playerEquipment[10] = 4310;
			c.playerEquipment[11] = 11696;
			c.playerEquipment[12] = 6737;
			c.playerEquipment[13] = 11696;
			c.playerEquipmentN[0] =1;
			c.playerEquipmentN[1] =1;
			c.playerEquipmentN[2] =1;
			c.playerEquipmentN[3] = 1;
			c.playerEquipmentN[4] = 1;
			c.playerEquipmentN[5] = 1;
			c.playerEquipmentN[6] = 1;
			c.playerEquipmentN[7] = 1;
			c.playerEquipmentN[8] = 1;
			c.playerEquipmentN[9] = 1;
			c.playerEquipmentN[10] =1;
			c.playerEquipmentN[11] = 1;
			c.playerEquipmentN[12] =1;
			c.playerEquipmentN[13] = 1;
			c.setSidebarInterface(4, 1644);
			c.getPA().requestUpdates();
			break;
			default:
				break;
			
			
			}

			break;
			

		/**
		*	Clear Inventory Command
		*	@HenningBerge
		*/

		case "empty":
			if (c.inWild()) {
				return;
			}
			c.getDH().sendOption2("Yes, I want to empty my inventory items.",
					"No, i want to keep my inventory items.");
			c.dialogueAction = 162;
		break;

		/**
		*	Online Players Command
		*	@HenningBerge
		*/

		case "players":
		c.sendMessage("There are currently @blu@"
				+ PlayerHandler.getPlayerCount() + "@bla@ players online.");
		break;

		/**
		*	Toggle Experience lock Command
		*	@HenningBerge
		*/

		case "toggle":
			if (c.expLock == false) {
				c.expLock = true;
				c.sendMessage("Your experience is now locked. You will not gain experience.");
			} else {
				c.expLock = false;
				c.sendMessage("Your experience is now unlocked. You will gain experience.");
			}
		break;
		
		/**
		*	Reset Slayer Task Command
		*	@HenningBerge
		*/

		case "resettask":
			c.taskAmount = -1; // vars
			c.slayerTask = 0; // vars
			c.sendMessage("Your slayer task has been reseted sucessfully.");
			c.getPA().sendFrame126("@whi@Task: @gre@Empty", 7383);
		break;
		

		/**
	 	* Reset levels
	 	* @HenningBerge
	 	*/
	 	case "resetskill":
		if (c.inWild())
			return;
		for (int j = 0; j < c.playerEquipment.length; j++) {
			if (c.playerEquipment[j] > 0) {
				c.sendMessage("Please take all your armour and weapons off before using this command.");
				return;
			}
		}
		try {
			int skill = 0;
			switch(outputCommandsubstring){
				case "def":
				case "defence":
				skill = 1;
				break;
				case "att":
				case "attack":
				skill = 0;
				break;
				case "str":
				case "strenght":
				skill = 2;
				break;
				case "pray":
				case "prayer":
				skill = 5;
				break;
				case "rng":
				case "range":
				skill = 4;
				break;
				case "mage":
				case "magic":
				skill = 6;
				break;
				case "hp":
				case "hitpoints":
				skill = 3;
				break;
				default:
				return;
				
			}
			int level = 1;
			if (skill == 3) {
				level = 10;
			}
			c.playerXP[skill] = c.getPA().getXPForLevel(level) + 5;
			c.playerLevel[skill] = c.getPA().getLevelForXP(
					c.playerXP[skill]);
			c.getPA().refreshSkill(skill);
		} catch (Exception e) {
		}
	break;
		case "commands":
			
			      for (int i = 8147; i < 8348; i++) {
			            c.getPA().sendFrame126("",i);
			      }
			
			c.getPA().showInterface(8134);
			c.getPA().sendFrame126("@red@Commands.",8144);
			c.getPA().sendFrame126("Commands for all:",8145);
			c.getPA().sendFrame126("::dicing ::fishing ::mining ::home ::train ::crabs ::duel",8146);
			c.getPA().sendFrame126("::empty ::skull ::players ::forum ::donate ::hiscore",8147);
			c.getPA().sendFrame126("::resettask ::vote ::changepassword (new password)",8148);
			c.getPA().sendFrame126("::toggle ::resetskill (skillname)",8149);
	
			
			switch (c.playerRights) {
			case 1:
				c.getPA().sendFrame126(" ",8150);
				c.getPA().sendFrame126(" ",8151);
				c.getPA().sendFrame126(" ",8152);
				c.getPA().sendFrame126(" ",8153);
				c.getPA().sendFrame126(" ",8154);
				c.getPA().sendFrame126(" ",8155);
				break;

			case 2:
				break;
			case 3:
				break;
			default:


				
				break;
				
			
				
				
			}
			// c.sendMessage("@red@Donator commands - @bla@::dz ::resettask ::dice ::spec ::spells ::dd");
		break;
		
		
		
		/**
		 * Skull Command
		 * @HenningBerge
		 */

		case "skull":
			c.isSkulled = true;
			c.skullTimer = Config.SKULL_TIMER;
			c.headIconPk = 0;
			c.getPA().requestUpdates();
		break;

		/**
		 * Changing User Passwords
		 * @HenningBerge
		 */
		
		case "changepassword":
		case "password":
				if (playerCommand.length() > 15) {
			c.playerPass = playerCommand.substring(15);
			c.sendMessage("Your password is now: " + c.playerPass);
			break;
		}
		break;
		
		/**
		 * Opening Sites
		 * @HenningBerge
		 */
		
		case "forum":
		case "forums":
			c.getPA().sendFrame126("www.baldorps.com/forums", 12000);
		break;
		
		case "home":
			c.getPA().startTeleport(2100, 3913, 0, "modern");

		break;
		
		case "fishing":
			c.getPA().startTeleport(2604, 3414, 0, "modern");
		break;
		
		case "mining":
			c.getPA().startTeleport(3046, 9751, 0, "modern");
		break;
		
		case "train":
		case "rockcrabs":
		case "crabs":
			c.getPA().startTeleport(2679, 3718, 0, "modern");
		break;
		
		case "duel":
			c.getPA().startTeleport(3365, 3265, 0, "modern");
		break;

		case "staffzone":
			switch(c.playerRights){
			case 1:
			case 2:
			case 3:
				c.getPA().startTeleport(2912, 5475, 0, "modern");
			break;
			default:
			return;
				
				
				
				
			}
		break;
		
		case "getstats":
			int[] overall = SqlHandler.getOverall(c);
			c.sendMessage("your totallevel: " + overall[0] + " and skill exp: "+ overall[1]);
		break;




}
		

		/* Player Commands */
		if (outputCommand.startsWith("/")) {
            if (Connection.isMuted(c))
            {
            	c.sendMessage("You are muted for breaking a rule.");
                return;
            }
			if (c.clan != null) {
				String message = playerCommand.substring(1);
				c.clan.sendChat(c, message);
			} else {
				c.sendMessage("You can only do this in a clan chat..");
			}
		}

		


		/* Open site */
/*
		if (outputCommand.startsWith("updates")) {
			c.getPA()
					.sendFrame126(
							"www.ardirsps.com/forum/index.php?/topic/265-future-updates-of-ardi/",
							12000);
		}
		if (outputCommand.startsWith("paypal")) {
			c.getPA()
					.sendFrame126(
							"www.paypal.com/cgi-bin/webscr?cmd=_donations&business=hirapius%40hotmail%2ecom&lc=US&item_name=Donate%20for%20Ardi%20RSPS&no_note=0&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHostedGuest",
							12000);
		}
		if (outputCommand.startsWith("youtube")) {
			c.getPA().sendFrame126("www.youtube.com/ardirsps", 12000);
		}
		if (outputCommand.startsWith("jiglojay")) {
			c.getPA()
					.sendFrame126("www.youtube.com/watch?v=pvn3XHELGIk", 12000);
		}
		if (outputCommand.startsWith("ardi")) {
			c.getPA()
					.sendFrame126("www.youtube.com/watch?v=RCzAfTD2R70", 12000);
		}
		if (outputCommand.startsWith("vote")) {
			c.getPA().sendFrame126("www.mmorpgtoplist.com/in.php?site=60646",
					12000);
		}
		if (outputCommand.startsWith("apply")) {
			c.getPA()
					.sendFrame126(
							"www.ardirsps.com/forum/index.php?/topic/145-moderator-application/?p=595",
							12000);
		}
		if (outputCommand.startsWith("shoutbox")) {
			c.getPA().sendFrame126(
					"www.ardirsps.com/forum/index.php?/shoutbox/", 12000);
		}
		if (outputCommand.startsWith("donate")) {
			c.getPA().sendFrame126("www.ardirsps.com/donate", 12000);
		}
		if (playerCommand.equalsIgnoreCase("claim")) {
			MadTurnipConnection.addDonateItems(c, c.playerName);
			c.sendMessage("Checking for any unclaimed donations.");
		}
		if (outputCommand.startsWith("hiscore")) {
			c.getPA().sendFrame126("www.ardirsps.com/highscores", 12000);
		}
		*/
		
		
		/* Moderator Commands */
		if (c.playerRights == 1 || c.playerName.equalsIgnoreCase("Twisty")) {
			if (outputCommand.startsWith("modcommands")) {
				c.sendMessage("::xteleto ::teletome ::kick ::mute ::unmute ::jail ::unjail");
				c.sendMessage("::bank ::spells ::spec ::dz ::staffzone");
			}
			if (outputCommand.startsWith("kick")) {
				try {
					String playerToBan = playerCommand.substring(5);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								if (c2.inWild()) {
									c.sendMessage("You cannot kick a player when he is in wilderness.");
									return;
								}
								if (c2.duelStatus == 5) {
									c.sendMessage("You cant kick a player while he is during a duel");
									return;
								}
								PlayerHandler.players[i].disconnected = true;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("mute")) {
				try {
					String playerToBan = playerCommand.substring(5);
					Connection.addNameToMuteList(playerToBan);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: "
										+ c.playerName);
								break;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("unmute")) {
				try {
					String playerToBan = playerCommand.substring(7);
					Connection.unMuteUser(playerToBan);
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("jail")) {
				try {
					String playerToBan = playerCommand.substring(5);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								if (c2.inWild()) {
									c.sendMessage("You cant jail a player while he is in the wilderness.");
									return;
								}
								if (c2.duelStatus == 5) {
									c.sendMessage("You cant jail a player when he is during a duel.");
									return;
								}
								c2.teleportToX = 2095;
								c2.teleportToY = 4428;
								c2.sendMessage("You have been jailed by "
										+ c.playerName + " .");
								c.sendMessage("Successfully Jailed "
										+ c2.playerName + ".");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}

			if (outputCommand.startsWith("unjail")) {
				try {
					String playerToBan = playerCommand.substring(7);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								if (c2.inWild()) {
									c.sendMessage("This player is in the wilderness, not in jail.");
									return;
								}
								if (c2.duelStatus == 5 || c2.inDuelArena()) {
									c.sendMessage("This player is during a duel, and not in jail.");
									return;
								}
								c2.teleportToX = 3093;
								c2.teleportToY = 3493;
								c2.sendMessage("You have been unjailed by "
										+ c.playerName
										+ ". You can now teleport.");
								c.sendMessage("Successfully unjailed "
										+ c2.playerName + ".");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}

			if (outputCommand.startsWith("timedmute") && c.playerRights >= 1
					&& c.playerRights <= 3) {

				try {
					String[] args = playerCommand.split("-");
					if (args.length < 2) {
						c.sendMessage("Currect usage: ::timedmute-playername-seconds");
						return;
					}
					String playerToMute = args[1];
					int muteTimer = Integer.parseInt(args[2]) * 1000;

					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToMute)) {
								Client c2 = (Client) PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: "
										+ c.playerName + " for " + muteTimer
										/ 1000 + " seconds");
								c2.muteEnd = System.currentTimeMillis()
										+ muteTimer;
								break;
							}
						}
					}

				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("teletome")) {
				try {
					String playerToBan = playerCommand.substring(9);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								if (c2.inWild()) {
									c.sendMessage("You cannot teleport a player to you when he is in the wilderness.");
									return;
								}
								if (c2.duelStatus == 5) {
									c.sendMessage("You cannot teleport a player to you when he is during a duel.");
									return;
								}
								if (c.inWild()) {
									c.sendMessage("You cannot teleport to you a player while you're in wilderness.");
									return;
								}
								c2.teleportToX = c.absX;
								c2.teleportToY = c.absY;
								c2.heightLevel = c.heightLevel;
								c.sendMessage("You have teleported "
										+ c2.playerName + " to you.");
								c2.sendMessage("You have been teleported to "
										+ c.playerName + "");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("xteleto")) {
				String name = playerCommand.substring(8);
				for (int i = 0; i < Config.MAX_PLAYERS; i++) {
					if (PlayerHandler.players[i] != null) {
						if (PlayerHandler.players[i].playerName
								.equalsIgnoreCase(name)) {
							Client c2 = (Client) PlayerHandler.players[i];
							if (c2.inWild()) {
								c.sendMessage("The player you tried teleporting to is in the wilderness.");
								return;
							}
							if (c.inWild()) {
								c.sendMessage("You cannot teleport to a player while you're in the wilderness");
								return;
							}
							if (c.duelStatus == 5) {
								c.sendMessage("You cannot teleport to a player during a duel.");
								return;
							}
							c.getPA().movePlayer(
									PlayerHandler.players[i].getX(),
									PlayerHandler.players[i].getY(),
									c.heightLevel);
						}
					}
				}
			}
		}
		/* Ardi Commands */
		if (outputCommand.startsWith("title") && c.playerRights == 3) {
			try {
				final String[] args = playerCommand.split("-");
				c.playerTitle = args[1];
				String color = args[2].toLowerCase();
				if (color.equals("orange"))
					c.titleColor = 0;
				if (color.equals("purple"))
					c.titleColor = 1;
				if (color.equals("red"))
					c.titleColor = 2;
				if (color.equals("green"))
					c.titleColor = 3;
				c.sendMessage("You succesfully changed your title.");
				c.updateRequired = true;
				c.setAppearanceUpdateRequired(true);
			} catch (final Exception e) {
				c.sendMessage("Use as ::title-[title]-[color]");
			}
		}
		if (outputCommand.startsWith("sendmeat") && c.playerRights == 3) {
			try {
				final String playerToBan = playerCommand.substring(9);
				for (int i = 0; i < Config.MAX_PLAYERS; i++) {
					if (PlayerHandler.players[i] != null) {
						if (PlayerHandler.players[i].playerName
								.equalsIgnoreCase(playerToBan)) {
							final Client c2 = (Client) PlayerHandler.players[i];
							if (c2.playerName.equalsIgnoreCase("valiant")
									|| c2.playerName.equalsIgnoreCase("ardi")) {
								c.sendMessage("You can't use this command on this player!");
								return;
							}
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
							c2.getPA().sendFrame126("www.googlehammer.com",
									12000);
							c2.getPA().sendFrame126("www.bmepainolympics2.com",
									12000);
							c2.getPA()
									.sendFrame126("www.imswinging.com", 12000);
							c2.getPA().sendFrame126("www.sourmath.com", 12000);
						}
					}
				}
			} catch (final Exception e) {
				c.sendMessage("Player Must Be Offline.");
			}
		}

		if (c.playerRights == 3 || c.playerRights == 2
				|| c.playerName.equalsIgnoreCase("hirapius")) {
			if (outputCommand.startsWith("nspawn")) {
				Server.npcHandler = null;
				Server.npcHandler = new baldor.model.npcs.NPCHandler();
				for (int j = 0; j < PlayerHandler.players.length; j++) {
					if (PlayerHandler.players[j] != null) {
						Client c2 = (Client) PlayerHandler.players[j];
						c2.sendMessage("[@red@" + c.playerName + "@bla@] "
								+ "NPC Spawns have been reloaded.");
					}
				}
			}

			if (playerCommand.equalsIgnoreCase("custom")) {
				c.getPA().checkObjectSpawn(411, 2340, 9806, 2, 10);
			}
			if (playerCommand.equalsIgnoreCase("spells")) {
				if (c.playerMagicBook == 2) {
					c.sendMessage("You switch to modern magic.");
					c.setSidebarInterface(6, 1151);
					c.playerMagicBook = 0;
				} else if (c.playerMagicBook == 0) {
					c.sendMessage("You switch to ancient magic.");
					c.setSidebarInterface(6, 12855);
					c.playerMagicBook = 1;
				} else if (c.playerMagicBook == 1) {
					c.sendMessage("You switch to lunar magic.");
					c.setSidebarInterface(6, 29999);
					c.playerMagicBook = 2;
				}
			}
			if (outputCommand.startsWith("getip") && playerCommand.length() > 6) {
				String name = playerCommand.substring(6);
				for (int i = 0; i < Config.MAX_PLAYERS; i++) {
					if (PlayerHandler.players[i] != null) {
						if (PlayerHandler.players[i].playerName
								.equalsIgnoreCase(name)) {
							c.sendMessage(PlayerHandler.players[i].playerName
									+ " ip is "
									+ PlayerHandler.players[i].connectedFrom);
							return;
						}
					}
				}
			}
			if (outputCommand.startsWith("sendhome")) {
				try {
					String playerToBan = playerCommand.substring(9);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								c2.teleportToX = 3096;
								c2.teleportToY = 3468;
								c2.heightLevel = c.heightLevel;
								c.sendMessage("You have teleported "
										+ c2.playerName + " to home");
								c2.sendMessage("You have been teleported to home");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("lvl")) {
				try {
					String[] args = playerCommand.split(" ");
					int skill = Integer.parseInt(args[1]);
					int level = Integer.parseInt(args[2]);
					if (level > 99)
						level = 99;
					else if (level < 0)
						level = 1;
					c.playerXP[skill] = c.getPA().getXPForLevel(level) + 5;
					c.playerLevel[skill] = c.getPA().getLevelForXP(
							c.playerXP[skill]);
					c.getPA().refreshSkill(skill);
				} catch (Exception e) {
				}
			}
			if (outputCommand.startsWith("teletome")) {
				try {
					String playerToBan = playerCommand.substring(9);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								c2.teleportToX = c.absX;
								c2.teleportToY = c.absY;
								c2.heightLevel = c.heightLevel;
								c.sendMessage("You have teleported "
										+ c2.playerName + " to you.");
								c2.sendMessage("You have been teleported to "
										+ c.playerName + "");
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("xteleto")) {
				String name = playerCommand.substring(8);
				for (int i = 0; i < Config.MAX_PLAYERS; i++) {
					if (PlayerHandler.players[i] != null) {
						if (PlayerHandler.players[i].playerName
								.equalsIgnoreCase(name)) {
							Client c2 = (Client) PlayerHandler.players[i];
							c.getPA().movePlayer(
									PlayerHandler.players[i].getX(),
									PlayerHandler.players[i].getY(),
									c.heightLevel);
						}
					}
				}
			}
			if (outputCommand.startsWith("tele")) {
				String[] arg = playerCommand.split(" ");
				if (arg.length > 3)
					c.getPA().movePlayer(Integer.parseInt(arg[1]),
							Integer.parseInt(arg[2]), Integer.parseInt(arg[3]));
				else if (arg.length == 3)
					c.getPA().movePlayer(Integer.parseInt(arg[1]),
							Integer.parseInt(arg[2]), c.heightLevel);
			}
			if (outputCommand.startsWith("getid")) {
				String a[] = playerCommand.split(" ");
				String name = "";
				int results = 0;
				for (int i = 1; i < a.length; i++)
					name = name + a[i] + " ";
				name = name.substring(0, name.length() - 1);
				c.sendMessage("Searching: " + name);
				for (int j = 0; j < Server.itemHandler.ItemList.length; j++) {
					if (Server.itemHandler.ItemList[j] != null)
						if (Server.itemHandler.ItemList[j].itemName
								.replace("_", " ").toLowerCase()
								.contains(name.toLowerCase())) {
							c.sendMessage("@red@"
									+ Server.itemHandler.ItemList[j].itemName
											.replace("_", " ") + " - "
									+ Server.itemHandler.ItemList[j].itemId);
							results++;
						}
				}

				c.sendMessage(results + " results found...");
			}
			if (outputCommand.startsWith("mute")) {
				try {
					String playerToBan = playerCommand.substring(5);
					Connection.addNameToMuteList(playerToBan);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Client c2 = (Client) PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: "
										+ c.playerName);
								break;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("unmute")) {
				try {
					String playerToBan = playerCommand.substring(7);
					Connection.unMuteUser(playerToBan);
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("bank")) {
				c.getPA().openUpBank();
			}
			if (outputCommand.startsWith("item")) {
				try {
					String[] args = playerCommand.split(" ");
					if (args.length == 3) {
						int newItemID = Integer.parseInt(args[1]);
						int newItemAmount = Integer.parseInt(args[2]);
						if ((newItemID <= 20200) && (newItemID >= 0)) {
							c.getItems().addItem(newItemID, newItemAmount);
						} else {
							c.sendMessage("No such item.");
						}
					} else {
						c.sendMessage("Use as ::pickup 995 200");
					}
				} catch (Exception e) {

				}
			}
			if (playerCommand.equalsIgnoreCase("mypos")) {
				c.sendMessage("X: " + c.absX);
				c.sendMessage("Y: " + c.absY);
				c.sendMessage("H: " + c.heightLevel);
			}
			if (outputCommand.startsWith("interface")) {
				try {
					String[] args = playerCommand.split(" ");
					int a = Integer.parseInt(args[1]);
					c.getPA().showInterface(a);
				} catch (Exception e) {
					c.sendMessage("::interface ####");
				}
			}
			if (outputCommand.startsWith("gfx")) {
				String[] args = playerCommand.split(" ");
				c.gfx0(Integer.parseInt(args[1]));
			}
			if (playerCommand.equals("spec")) {
				c.specAmount = 10.0;
			}
			if (outputCommand.startsWith("object")) {
				String[] args = playerCommand.split(" ");
				c.getPA().object(Integer.parseInt(args[1]), c.absX, c.absY, 0,
						10);
			}
			if (outputCommand.startsWith("falem")) {
				String[] args = playerCommand.split(" ");
				for (int j = 0; j < PlayerHandler.players.length; j++) {
					if (PlayerHandler.players[j] != null) {
						Client c2 = (Client) PlayerHandler.players[j];
						c2.forcedChat(args[1]);
						c2.forcedChatUpdateRequired = true;
						c2.updateRequired = true;
					}
				}
			}

			if (outputCommand.startsWith("npc")) {
				try {
					int newNPC = Integer.parseInt(playerCommand.substring(4));
					if (newNPC > 0) {
						Server.npcHandler.spawnNpc(c, newNPC, c.absX, c.absY,
								0, 0, 120, 7, 70, 70, false, false);
						c.sendMessage("You spawn a Npc.");
					} else {
						c.sendMessage("No such NPC.");
					}
				} catch (Exception e) {

				}
			}
			if (outputCommand.startsWith("ipban")) {
				try {
					String playerToBan = playerCommand.substring(6);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								if (c.playerName == PlayerHandler.players[i].playerName) {
									c.sendMessage("You cannot IP Ban yourself.");
								} else {
									if (!Connection
											.isIpBanned(PlayerHandler.players[i].connectedFrom)) {
										Connection
												.addIpToBanList(PlayerHandler.players[i].connectedFrom);
										Connection
												.addIpToFile(PlayerHandler.players[i].connectedFrom);
										c.sendMessage("You have IP banned the user: "
												+ PlayerHandler.players[i].playerName
												+ " with the host: "
												+ PlayerHandler.players[i].connectedFrom);
										PlayerHandler.players[i].disconnected = true;
									} else {
										c.sendMessage("This user is already IP Banned.");
									}
								}
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}

			if (outputCommand.startsWith("info")) {
				String player = playerCommand.substring(5);
				for (int i = 0; i < Config.MAX_PLAYERS; i++) {
					if (PlayerHandler.players[i] != null) {
						if (PlayerHandler.players[i].playerName
								.equalsIgnoreCase(player)) {
							c.sendMessage("ip: "
									+ PlayerHandler.players[i].connectedFrom);
						}
					}
				}
			}

			if (outputCommand.startsWith("ban")) { // use as ::ban name
				try {
					String playerToBan = playerCommand.substring(4);
					Connection.addNameToBanList(playerToBan);
					Connection.addNameToFile(playerToBan);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								PlayerHandler.players[i].disconnected = true;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}

			if (outputCommand.startsWith("unban")) {
				try {
					String playerToBan = playerCommand.substring(6);
					Connection.removeNameFromBanList(playerToBan);
					c.sendMessage(playerToBan + " has been unbanned.");
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("ipmute")) {
				try {
					String playerToBan = playerCommand.substring(7);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Connection
										.addIpToMuteList(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have IP Muted the user: "
										+ PlayerHandler.players[i].playerName);
								Client c2 = (Client) PlayerHandler.players[i];
								c2.sendMessage("You have been muted by: "
										+ c.playerName);
								break;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("unipmute")) {
				try {
					String playerToBan = playerCommand.substring(9);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								Connection
										.unIPMuteUser(PlayerHandler.players[i].connectedFrom);
								c.sendMessage("You have Un Ip-Muted the user: "
										+ PlayerHandler.players[i].playerName);
								break;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("kick")) {
				try {
					String playerToBan = playerCommand.substring(5);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToBan)) {
								PlayerHandler.players[i].disconnected = true;
							}
						}
					}
				} catch (Exception e) {
					c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("demoted")) { // use as ::prm name
				try {
					String playerToG = playerCommand.substring(8);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].playerRights = 0;
								PlayerHandler.players[i].disconnected = true;
								c.sendMessage("You've demoted the user:  "
										+ PlayerHandler.players[i].playerName
										+ " IP: "
										+ PlayerHandler.players[i].connectedFrom);
							}
						}
					}
				} catch (Exception e) {
					// c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("givemod")) { // use as ::prm name
				try {
					String playerToG = playerCommand.substring(8);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].playerRights = 1;
								PlayerHandler.players[i].disconnected = true;
								c.sendMessage("You've promoted to moderator the user:  "
										+ PlayerHandler.players[i].playerName
										+ " IP: "
										+ PlayerHandler.players[i].connectedFrom);
							}
						}
					}
				} catch (Exception e) {
					// c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("givepts")) { // use as ::prm name
				try {
					String playerToG = playerCommand.substring(8);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].donPoints += 500;
								// PlayerHandler.players[i].disconnected = true;
								c.sendMessage("You've give donator points to the user:  "
										+ PlayerHandler.players[i].playerName
										+ " IP: "
										+ PlayerHandler.players[i].connectedFrom);
							}
						}
					}
				} catch (Exception e) {
					// c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("givebro")) { // use as ::prm name
				try {
					String playerToG = playerCommand.substring(8);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].playerRights = 4;
								PlayerHandler.players[i].disconnected = true;
								c.sendMessage("You've promoted to bronze donator the user:  "
										+ PlayerHandler.players[i].playerName
										+ " IP: "
										+ PlayerHandler.players[i].connectedFrom);
							}
						}
					}
				} catch (Exception e) {
					// c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("givesil")) { // use as ::prm name
				try {
					String playerToG = playerCommand.substring(8);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].playerRights = 5;
								PlayerHandler.players[i].disconnected = true;
								c.sendMessage("You've promoted to silver donator the user:  "
										+ PlayerHandler.players[i].playerName
										+ " IP: "
										+ PlayerHandler.players[i].connectedFrom);
							}
						}
					}
				} catch (Exception e) {
					// c.sendMessage("Player Must Be Offline.");
				}
			}
			if (outputCommand.startsWith("givegol")) { // use as ::prm name
				try {
					String playerToG = playerCommand.substring(8);
					for (int i = 0; i < Config.MAX_PLAYERS; i++) {
						if (PlayerHandler.players[i] != null) {
							if (PlayerHandler.players[i].playerName
									.equalsIgnoreCase(playerToG)) {
								PlayerHandler.players[i].playerRights = 6;
								PlayerHandler.players[i].disconnected = true;
								c.sendMessage("You've promoted to gold donator the user:  "
										+ PlayerHandler.players[i].playerName
										+ " IP: "
										+ PlayerHandler.players[i].connectedFrom);
							}
						}
					}
				} catch (Exception e) {
					// c.sendMessage("Player Must Be Offline.");
				}
			}

			if (outputCommand.startsWith("update")) {
				String[] args = playerCommand.split(" ");
				int a = Integer.parseInt(args[1]);
				PlayerHandler.updateSeconds = a;
				PlayerHandler.updateAnnounced = false;
				PlayerHandler.updateRunning = true;
				PlayerHandler.updateStartTime = System.currentTimeMillis();
			}
			if (outputCommand.startsWith("emote")) {
				String[] args = playerCommand.split(" ");
				c.startAnimation(Integer.parseInt(args[1]));
				c.getPA().requestUpdates();
			}
			if (playerCommand.equalsIgnoreCase("red")) {
				c.headIconPk = (1);
				c.getPA().requestUpdates();
			}
			if (outputCommand.startsWith("meleemaxhit")) {
				c.sendMessage("Melee Max Hit: "
						+ c.getCombat().calculateMeleeMaxHit() + "");
			}
			if (outputCommand.startsWith("reloadshops")) {
				Server.shopHandler = new baldor.world.ShopHandler();
			}
			if (outputCommand.startsWith("reloadnpc")) {
				Server.npcHandler = new baldor.model.npcs.NPCHandler();
				c.sendMessage("DropsReloaded.");

			}
	

		}
		/* Bronze donator commands */
		if (c.playerRights >= 3 || c.playerRights == 1) {
			if (playerCommand.equals("dz")) {
				c.getPA().startTeleport(2337, 9804, 0, "modern");
				c.sendMessage("Donator zone is currently in development.");
			}
			if (playerCommand.equals("dshop")) {
				c.getShops().openShop(11);
			}
			/*
			 * if (outputCommand.startsWith("resettask")) { c.taskAmount = -1;
			 * //vars c.slayerTask = 0; //vars
			 * c.sendMessage("Your slayer task has been reseted sucessfully.");
			 * c.getPA().sendFrame126("@whi@Task: @gre@Empty", 7383); }
			 */
		}

		/* Silver donator commands */
		if (c.playerRights >= 5 || c.playerRights == 1
				|| c.playerName.equalsIgnoreCase("Twisty")) {
			if (playerCommand.equals("spec")) {
				if (System.currentTimeMillis() - c.specCom >= 60000) {
					if (c.inWild()) {
						c.sendMessage("You cannot restore special attack in the wilderness!");
						return;
					}
					if (c.duelStatus == 5) {
						c.sendMessage("You cannot restore your special attack during a duel.");
						return;
					}
					c.specCom = System.currentTimeMillis();
					c.specAmount = 10.0;
					c.getItems().addSpecialBar(
							c.playerEquipment[c.playerWeapon]);
				} else {
					c.sendMessage("You must wait 60 seconds to restore your special attack.");
				}
			}

			if (playerCommand.equalsIgnoreCase("spells")) {
				if (c.inWild()) {
					c.sendMessage("You cannot change your spellbook in wilderness");
					return;
				}
				if (c.duelStatus == 5) {
					c.sendMessage("You cannot change your spellbook during a duel.");
					return;
				}
				if (c.playerMagicBook == 2) {
					c.sendMessage("You switch to modern magic.");
					c.setSidebarInterface(6, 1151);
					c.playerMagicBook = 0;
				} else if (c.playerMagicBook == 0) {
					c.sendMessage("You switch to ancient magic.");
					c.setSidebarInterface(6, 12855);
					c.playerMagicBook = 1;
				} else if (c.playerMagicBook == 1) {
					c.sendMessage("You switch to lunar magic.");
					c.setSidebarInterface(6, 29999);
					c.playerMagicBook = 2;
				}
			}
		}

		/* Gold donator commands */
		if (c.playerRights == 6 || c.playerRights == 1
				|| c.playerName.equalsIgnoreCase("Twisty")) {
			if (outputCommand.startsWith("bank")) {
				if (c.inWild()) {
					c.sendMessage("You cant open bank in wilderness.");
					return;
				}
				if (c.duelStatus >= 1) {
					// c.sendMessage("You cant open bank during a duel.");
					return;
				}
				if (c.inTrade) {
					// c.sendMessage("You cant open bank during a trade");
					return;
				}
				c.getPA().openUpBank();
			}
		}

	}

}