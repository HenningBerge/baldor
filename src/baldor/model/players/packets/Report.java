package baldor.model.players.packets;

import baldor.model.players.Client;
import baldor.model.players.PacketType;

public class Report implements PacketType {

	public void processPacket(Client c, int packetType, int packetSize) {
		try {
			ReportHandler.handleReport(c);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}