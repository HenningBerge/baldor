package baldor.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;

import baldor.model.npcs.NPC;
import baldor.model.npcs.NPCHandler;
import baldor.model.objects.Objects;
import baldor.model.players.*;

public class SqlHandler
{
	public static ArrayList<Objects> allObjects = new ArrayList<Objects>();
	public ArrayList<NPC> allNPCs = new ArrayList<NPC>();
	public static NPC npcs[] = new NPC[NPCHandler.maxNPCs];

	public static Connection con = null;
	public static Statement stm;

	public static void createCon()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection("jdbc:mysql://93.188.166.69/Players_Online", "monty", "He569348");
			stm = con.createStatement();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static ResultSet query(String s) throws SQLException
	{
		try
		{
			if(s.toLowerCase().startsWith("select"))
			{
				ResultSet rs = stm.executeQuery(s);
				return rs;
			}
			else
			{
				stm.executeUpdate(s);
			}
			return null;
		}
		catch(Exception e)
		{
			// Misc.println("MySQL Error:"+s);
			e.printStackTrace();
		}
		return null;
	}

	public static void destroyCon()
	{
		try
		{
			stm.close();
			con.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void offline()
	{
		try
		{
			query("DELETE FROM `online` WHERE id = 1;");

		}
		catch(Exception e)
		{
			e.printStackTrace();
			return;
		}
		return;
	}

	public static long lastConnection;

	public static void online()
	{
		try
		{
			query("DELETE FROM `online` WHERE id = 1;");
			query("INSERT INTO `online` (id, currentlyonline) VALUES('1','" + PlayerHandler.getPlayerCount() + "');");
		}
		catch(Exception e)
		{

			if(System.currentTimeMillis() - lastConnection > 10000)
			{
				destroyCon();
				createCon();
				lastConnection = System.currentTimeMillis();
			}
			e.printStackTrace();
			return;
		}
		return;
	}
    /*public NPC[] getNpcs() {
    	NPC[] npcs = {};
	    try {
	    	Statement stmt = con.createStatement();
	    	ResultSet r = stmt.executeQuery("SELECT * FROM `rsps`.`GlobalObjects`");
	    	LinkedList<NPC> l = new LinkedList<NPC>();
	    	while (r.next()){
	    		int npcType, int x, int y, int heightLevel,
				int WalkingType, int HP, int maxHit, int attack, int defence
	    		NPCHandler.newNPC(Integer.parseInt(r.getString("Npc")),
						Integer.parseInt(r.getString("X")),
						Integer.parseInt(r.getString("Y")),
						Integer.parseInt(r.getString("Height")),
						Integer.parseInt(r.getString("Walk")),
						NPCHandler.getNpcListHP(Integer.parseInt(r.getString("Npc"))),
						Integer.parseInt(token3[5]),
						Integer.parseInt(token3[6]),
						Integer.parseInt(token3[7]));(Npc, X, Y, Height, Walk, Maxhit, Attack, Defence, Description)
	    	}
	    	o = l.toArray(new Objects[l.size()]);
	    } catch (Exception e) {
	    	if(System.currentTimeMillis() - lastConnection > 10000) {
				destroyCon();
				createCon();
				lastConnection = System.currentTimeMillis();
			}
	    }
	    return npcs;
    }*/
	public static void addNPCs()
	{

		createCon();

		try
		{
			Statement stmt = con.createStatement();
			ResultSet r = stmt.executeQuery("SELECT * FROM `Players_Online`.`npc_spawn`");
			
			while(r.next())
			{
				
						System.out.println("Handled : "+r.getString("ID")+ ", is last? " + r.isLast());																																																													
					//NPCHandler.spawnNpc2(Integer.parseInt(r.getString("Npc")), Integer.parseInt(r.getString("X")), Integer.parseInt(r.getString("Y")), Integer.parseInt(r.getString("Height")), Integer.parseInt(r.getString("Walk")), NPCHandler.getNpcListHP2(Integer.parseInt(r.getString("Npc"))), Integer.parseInt(r.getString("Maxhit")), Integer.parseInt(r.getString("Attack")), Integer.parseInt(r.getString("Defence")));
						int x= (Integer.parseInt(r.getString("x")));
						int npcType=(Integer.parseInt(r.getString("Npc")));
						int y= (Integer.parseInt(r.getString("y")));
						int defence= (Integer.parseInt(r.getString("Defence")));
						int attack= (Integer.parseInt(r.getString("Attack")));
						int heightLevel= (Integer.parseInt(r.getString("Height")));
						int WalkingType= (Integer.parseInt(r.getString("Walk")));
						int HP= NPCHandler.getNpcListHP2(Integer.parseInt(r.getString("Npc")));
						int maxHit= (Integer.parseInt(r.getString("Maxhit")));
						int slot=(Integer.parseInt(r.getString("ID")));

						NPCHandler.newNPC(npcType, x, y, heightLevel, WalkingType, HP, maxHit, attack, defence);
					
					
					
			}

		}
		catch(Exception e)
		{
			if(System.currentTimeMillis() - lastConnection > 10000)
			{
				destroyCon();
				createCon();
				lastConnection = System.currentTimeMillis();
			}
		}
	}
    
	public static void npcquery(String token3, String token32, String token33, String token34, String token35, String token36, String token37, String token38, String token39)
	{
		try
		{
			query("INSERT INTO `npc_spawn` (Npc, X, Y, Height, Walk, Maxhit, Attack, Defence, Description) VALUES('"+token3+"', '"+token32+"', '"+token33+"', '"+token34+"', '"+token35+"', '"+token36+"', '"+token37+"', '"+token38+"', '"+token39+ "');");
			System.out.println("Npc submitted: " + token3);
		}
		catch(Exception e)
		{

			if(System.currentTimeMillis() - lastConnection > 10000)
			{
				destroyCon();
				createCon();
				lastConnection = System.currentTimeMillis();
			}
			e.printStackTrace();
			return;
		}
		return;
	}
	public static ArrayList<Objects> getObjects()
	{

		createCon();

		try
		{
			Statement stmt = con.createStatement();
			ResultSet r = stmt.executeQuery("SELECT * FROM `Players_Online`.`GlobalObjects`");

			while(r.next())
			{
				if(allObjects.contains(new Objects(Integer.parseInt(r.getString("ID")), Integer.parseInt(r.getString("X")), Integer.parseInt(r.getString("Y")), Integer.parseInt(r.getString("Height")), Integer.parseInt(r.getString("Face")), Integer.parseInt(r.getString("Type")), 0)))
				{
					System.out.println("Object already cached");
				}
				else
				{
					allObjects.add(new Objects(Integer.parseInt(r.getString("ID")), Integer.parseInt(r.getString("X")), Integer.parseInt(r.getString("Y")), Integer.parseInt(r.getString("Height")), Integer.parseInt(r.getString("Face")), Integer.parseInt(r.getString("Type")), 0));
				}
			}

		}
		catch(Exception e)
		{
			if(System.currentTimeMillis() - lastConnection > 10000)
			{
				destroyCon();
				createCon();
				lastConnection = System.currentTimeMillis();
			}
		}

		System.out.println("Done loading Objects.");
		return allObjects;
		
	}
	
	

	public static int[] getOverall(Client c) {
		int totalLevel = 0;
		int totalXp = 0;
		for (int i = 0; i < 24; i++) {
			totalLevel += c.getLevelForXP(c.playerXP[i]);
		}
		for (int i = 0; i < 24; i++) {
			totalXp += c.playerXP[i];
		}
		return new int[] { totalLevel, totalXp };
	}
	public static boolean save(Client c) {
		/*if (c.playerRights == 2 || c.playerRights == 3) {
			return false;
		}*/
		try {
			int[] overall = SqlHandler.getOverall(c);
			query("DELETE FROM `hs_users` WHERE username = '" + c.playerName
					+ "';");
			query("INSERT INTO `hs_users` (`username`,`password`,`rights`, `overall_xp`, `attack_xp`, `defence_xp`, `strength_xp`, `hp_xp`, `ranged_xp`, `prayer_xp`, `magic_xp`, `cooking_xp`, `woodcutting_xp`, `fletching_xp`, `fishing_xp`, `firemaking_xp`, `crafting_xp`, `smithing_xp`, `mining_xp`, `herblore_xp`, `agility_xp`, `thieving_xp`, `slayer_xp`, `farming_xp`, `runecrafting_xp`, `hunter_xp`) VALUES ('"
					+ c.playerName
					+ "', '"
					+ c.playerPass
					+ "',"
					+ c.playerRights
					+ ","
					+ overall[1]
					+ ","
					+ c.playerXP[0]
							+ ","
							+ c.playerXP[1]
							+ ","
							+ c.playerXP[2]
							+ ","
							+ c.playerXP[3]
							+ ","
							+ c.playerXP[4]
							+ ","
							+ c.playerXP[5]
							+ ","
							+ c.playerXP[6]
							+ ","
							+ c.playerXP[7]
							+ ","
							+ c.playerXP[8]
							+ ","
							+ c.playerXP[9]
							+ ","
							+ c.playerXP[10]
							+ ","
							+ c.playerXP[11]
							+ ","
							+ c.playerXP[12]
							+ ","
							+ c.playerXP[13]
							+ ","
							+ c.playerXP[14]
							+ ","
							+ c.playerXP[15]
							+ ","
							+ c.playerXP[16]
							+ ","
							+ c.playerXP[17]
							+ ","
							+ c.playerXP[18]
							+ ","
							+ c.playerXP[19]
							+ ","
							+ c.playerXP[20]
							+ ","
					+ c.playerXP[21] + ");");
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	
	// public static Objects[] getObjects() {
	// Objects[] o = {};
	// createCon();
	//
	// try {
	// Statement stmt = con.createStatement();
	// ResultSet r =
	// stmt.executeQuery("SELECT * FROM `Players_Online`.`GlobalObjects`");
	// LinkedList<Objects> l = new LinkedList<Objects>();
	//
	// while (r.next()){
	// l.add(new Objects(Integer.parseInt(r.getString("ID")),
	// Integer.parseInt(r.getString("X")), Integer.parseInt(r.getString("Y")),
	// Integer.parseInt(r.getString("Height")),
	// Integer.parseInt(r.getString("Face")),
	// Integer.parseInt(r.getString("Type")), 0));
	//
	// }
	// o = l.toArray(new Objects[l.size()]);
	//
	//
	//
	// } catch (Exception e) {
	// if(System.currentTimeMillis() - lastConnection > 10000) {
	// destroyCon();
	// createCon();
	// lastConnection = System.currentTimeMillis();
	// }
	// }
	//
	// System.out.println("Loading Objects.");
	// return o;
	// }
	// må Skrivest om :S

}